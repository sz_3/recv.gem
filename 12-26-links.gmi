# My useful links for Gemini setup

recv.cc's gemini server is running on AWS EC2 (on a microscopic `t4g.nano` instance). Setup was not too bad. Finding a guide on what to do was probably the hardest part.

=> gemini://gemini.circumlunar.space/ Autofill link to Gemini home (in case you forgot)
=> gemini://skyjake.fi/lagrange/ Lagrange (is the best Gemini GUI I've found thus far)
=> gemini://gem.limpet.net/agate/ Agate (static site gemini server written in rust)
=> gemini://shit.cx/tech/meta/2020-10-30-the-shit-cx-infra/ Example setup with Agate + scripts to generate a site
=> https://github.com/makeworld-the-better-one/md2gemini md2gemini (for markdown -> gmi)

I'll probably update this post with my own setup notes, once I've nailed everything down.

